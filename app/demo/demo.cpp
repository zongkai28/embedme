#include <zemb/BaseType.h>
#include <zemb/Tracer.h>

using namespace std;
using namespace zemb;

int main()
{
    Tracer::getInstance().setLevel(TRACE_LEVEL_DBG);
    Tracer::getInstance().addSink(std::make_shared<STDSink>()).start();
    TRACE_INFO("hello,world!");
    return 0;
}
