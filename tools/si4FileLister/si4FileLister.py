#!/usr/bin/env python3
import os
import sys
import shutil
import platform
from argparse import ArgumentParser

# 查找所有.o文件
def find_all_obj_file(srcDir):
    fileList=[]
    for root,dirs,files in os.walk(srcDir):
        for file in files:
            fullPath = root + "/"
            fullPath = fullPath + file
            if fullPath[-2:]=='.o':
                fileList.append(fullPath)
    return fileList

# 查找所有.c和.h文件
def find_all_src_file(objFileList):
    srcFileList = []
    for file in objFileList:
        cFile = file[:-2]+'.c'
        if os.path.exists(cFile):
            srcFileList.append(cFile)
        hFile = file[:-2]+'.h'
        if os.path.exists(hFile):
            srcFileList.append(hFile)
    return srcFileList

def build_si4_file_list(srcDir, outFileName,isWin):
    objFileList = find_all_obj_file(srcDir)
    srcFileList = find_all_src_file(objFileList)
    srcFullPath = os.path.dirname(srcDir+'/')
    srcParentPath = os.path.dirname(srcFullPath)
    pref = len(srcParentPath)
    with open(outFileName, 'w', encoding = 'utf-8') as f:
        for filePath in srcFileList:
            srcFilePath = filePath
            if pref>0:
                srcFilePath = filePath[pref+1:]    
            if isWin:            
                srcFilePath = srcFilePath.replace('/','\\')
            f.write(srcFilePath+'\n')

if __name__ == "__main__":
    srcDir = ''
    parser = ArgumentParser(description='args process.')
    parser.add_argument('win',nargs='?',help='build file list for windows platform')
    parser.add_argument('-s',help='source path')
    parser.add_argument('-o',help='file list output')
    args = parser.parse_args()
    isWin = True
    if args.win is None:
        if platform.system()!='Windows':
            isWin = False

    if args.s is None or args.o is None:
        sys.exit()

    srcDir = args.s
    outFileName = args.o
    if not os.path.exists(srcDir):
        print('not exist:',srcDir,'!!!')
        sys.exit()

    build_si4_file_list(srcDir, outFileName,isWin)
   