# 简介

此工程移植自sdbus-cpp项目,要求systemd版本最低为2.3.9。具体信息请参考官方说明。

项目地址：[https://github.com/Kistler-Group/sdbus-cpp](https://github.com/Kistler-Group/sdbus-cpp)

项目文档：[README.md](docs/README.md)