/******************************************************************************
 * This file is part of libemb.
 *
 * libemb is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libemb is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libemb.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: Embedme
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://gitee.com/newgolo/embedme.git
 * Copyright 2014~2020 @ ShenZhen ,China
*******************************************************************************/
#pragma once
#include <iostream>
#include <map>
#include <spdlog/logger.h>
#include "libemb/BaseType.h"
#include "libemb/Singleton.h"
#include "libemb/StrUtil.h"
#include "libemb/ThreadUtil.h"

namespace libembx{
using namespace libemb;
class SpdLogger:public Singleton<SpdLogger>{
DECL_CLASSNAME(SpdLogger)
DECL_SINGLETON(SpdLogger)
public:
    virtual ~SpdLogger();
    /**
     * @brief 创建日志
     * @param logID 日志ID
     * @param logFile 日志名称
     * @param limitSize 日志文件最大大小,默认为10M
     * @param rotations 循环日志文件个数,默认为0个
     * @return 
     */
    bool createLog(int logID, const std::string& logFile, uint32 limitSize=10485760, uint32 rotations=0);
    /**
     * @brief 记录INFO日志(普通日志)
     * @param logID 日志ID
     * @param info 日志内容
     */
    void logInfo(int logID, const std::string& info);
    /**
     * @brief 记录ERROR日志(错误日志)
     * @param logID 日志ID
     * @param error 日志内容
     */
    void logError(int logID, const std::string& error);
private:
    std::string m_logDir{"."};
    class Log{
    public:
        std::shared_ptr<spdlog::logger> m_logger;
        Mutex m_mutex;
    };
    std::map<int,std::shared_ptr<Log>> m_logMap;
};

}