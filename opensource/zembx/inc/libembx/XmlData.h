/******************************************************************************
 * This file is part of libemb.
 *
 * libemb is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libemb is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libemb.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: Embedme
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://gitee.com/newgolo/embedme.git
 * Copyright 2014~2020 @ ShenZhen ,China
*******************************************************************************/
#ifndef __XML_DATA_H__
#define __XML_DATA_H__

#define USE_TINYXML2    0   //使用timyxml2(由于tinyxml与tinyxml2接口变化较大,暂时没有移植完,请不要使用)
#if USE_TINYXML2
#include "tinyxml2.h"
using namespace tinyxml2;
using TiXmlNode = XMLNode;
using TiXmlDocument = XMLDocument;
using TiXmlElement = XMLElement;
using TiXmlText = XMLText;
using TiXmlPrinter = XMLPrinter;
#else
#include "tinyxml.h"
#endif
#include "libemb/BaseType.h"
namespace libembx{
class XmlData;
/**
 *  @file   XmlData.h   
 *  @class  XmlNode
 *  @brief  XML结点类	
 */
class XmlNode{
DECL_CLASSNAME(XmlNode)
public:
    XmlNode();
    ~XmlNode();
    bool isNullNode();
    int childNum();
    std::string serialize();
    XmlNode operator[](int idx);
    XmlNode operator[](std::string name);
    int toInt();
    double toDouble();
    std::string toString();
    bool setValue(const int& value);
    bool setValue(const double& value);
    bool setValue(const std::string& value);
    bool addSubNode(const std::string& name, const int& value);
    bool addSubNode(const std::string& name, const double& value);
    bool addSubNode(const std::string& name, const std::string value="");
    bool getAttribute(const std::string& attr, int& value);
    bool getAttribute(const std::string& attr, double& value);
    bool getAttribute(const std::string& attr, std::string& value);
    bool setAttribute(const std::string& attr, const int& value);
    bool setAttribute(const std::string& attr, const double& value);
    bool setAttribute(const std::string& attr, const std::string& value);
    std::string text();
    bool setText(const std::string& text);
private:
    friend class XmlData;
    TiXmlNode* m_root;
    std::string m_name;
};
/**
 *  @file   XmlData.h   
 *  @class  XmlData
 *  @brief  XML数据类	
 */
class XmlData{
DECL_CLASSNAME(XmlData)
public:
    XmlData();
    ~XmlData();
    bool initWithDataString(const std::string & content);
    bool initWithContentOfFile(const std::string& fileName);
    bool saveAsFile(const std::string& fileName);
    std::string serialize();
    bool addSubNode(const std::string& nodeName,const int& value);
    bool addSubNode(const std::string& nodeName,const double& value);
    bool addSubNode(const std::string& nodeName,const std::string value="");
    XmlNode operator[](std::string name);
private:
    TiXmlDocument m_document;
};
}
#endif