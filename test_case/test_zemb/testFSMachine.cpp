#include <zemb/BaseType.h>
#include <zemb/Tracer.h>
#include <zemb/ArgUtil.h>
#include <zemb/FSMachine.h>
#include <zemb/Thread.h>

using namespace zemb;
enum TestState{
	STATE_IDLE=0,
	STATE_START,
	STATE_STOP
};
class StateIdle:public FSHandler{
DECL_CLASSNAME(StateIdle)
public:
	int handleState()
	{
		if (m_circles--<0)
		{
			return -1;
		}
		TRACE_DBG_CLASS("enter IDLE state...");
		Thread::msleep(2000);
		TRACE_DBG_CLASS("exit IDLE state...");
		return STATE_START;
	}
private:
	int m_circles{2};
};

class StateStart:public FSHandler{
DECL_CLASSNAME(StateStart)
public:
	int handleState()
	{
		TRACE_DBG_CLASS("enter START state...");
		Thread::msleep(2000);
		TRACE_DBG_CLASS("exit START state...");
		return STATE_STOP;
	}
};

class StateStop:public FSHandler{
DECL_CLASSNAME(StateStop)
public:
	int handleState()
	{
		TRACE_DBG_CLASS("enter STOP state...");
		Thread::msleep(2000);
		TRACE_DBG_CLASS("exit STOP state...");
		return STATE_IDLE;
	}
};

class StateListener:public FSListener{
DECL_CLASSNAME(StateListener)
public:
	void onStateChanged(int stateID)
	{
		TRACE_DBG_CLASS("Aha! I get state changed: %d",stateID);
	}
};

void testFSMachine(void)
{
    TRACE_INFO("FSMachine test start >>>>>>");
	FSMachine machine;
	machine.registerState(STATE_IDLE,std::make_shared<StateIdle>());
	machine.registerState(STATE_START,std::make_shared<StateStart>());
	machine.registerState(STATE_STOP,std::make_shared<StateStop>());
	machine.registerListener(std::make_shared<StateListener>());
	machine.initState(STATE_IDLE);
	while(1)
	{
		machine.processState();
	}
    TRACE_INFO("FSMachine test finished <<<<<<");
}
