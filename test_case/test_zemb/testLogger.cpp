#include <zemb/BaseType.h>
#include <zemb/Tracer.h>
#include <zemb/ArgUtil.h>
#include <zemb/Logger.h>
using namespace zemb;

void testLogger(void)
{
    TRACE_INFO("Logger test start >>>>>>");
	LoggerManager::getInstance().setRoot("./log");
	if (!LoggerManager::getInstance().createLogger("test",512,3))
	{
		TRACE_ERR("create logger error!");
		return;
	}
	
	for(auto i=0; i<10000; i++)
	{
		LOG_INFO("test","this is a test log.");
	}

    TRACE_INFO("Logger test finished <<<<<<");
}
